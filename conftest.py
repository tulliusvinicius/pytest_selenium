import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service


driver: webdriver.Remote
service = Service(ChromeDriverManager().install())

@pytest.fixture
def setup_teardown():
    # setup
    global driver
    global service
    
    driver = webdriver.Chrome(service=service)
    driver.implicitly_wait(5)
    driver.maximize_window()
    url = 'https://www.saucedemo.com/'
    driver.get(url)

    # run test
    yield 

    # teardown
    driver.quit()
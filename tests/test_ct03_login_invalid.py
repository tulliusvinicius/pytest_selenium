from pages.login_page import LoginPage
import pytest


@pytest.mark.usefixtures("setup_teardown")
class TestCT03:

    def test_ct03_login_invalid(self):

        expected_text = 'Epic sadface: Username and password do not match any user in this service'
        # Instanciando objetos a serem usados no teste
        login_page = LoginPage()

        # Faz login
        login_page.fazer_login("standard_user", "senha_incorreta")

        # Verificar se o login não foi realizado e a mensagem de erro apareceu
        login_page.verificar_mensagem_erro_login_existe()

        # Verifica o texto da mensagem de erro
        login_page.verificar_texto_mensagem_erro_login(expected_text)

    
        #verificar mensagem de erro apresentada
        #actual_text = driver.find_element(By.XPATH, "//h3").text
        #assert actual_text == expected_text, f"Texto atual: '{actual_text}'. Texto esperado: '{expected_text}'"
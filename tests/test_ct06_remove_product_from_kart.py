from pages.login_page import LoginPage
from pages.home_page import HomePage
from pages.kart_page import KartPage
import pytest
import time


@pytest.mark.usefixtures("setup_teardown")
class TestCT06:

    def teste_ct06_remove_product_from_kart(self):

        login_page = LoginPage()
        home_page = HomePage()
        kart_page = KartPage()

        produto = "Sauce Labs Fleece Jacket"

        # fazer login
        login_page.fazer_login("standard_user", "secret_sauce")

        # clicar no produto e adicionar no carrinho
        home_page.adicionar_ao_carrinho(produto)

        # verificar que produto foi adicionado
        home_page.acessar_carrinho()
        kart_page.verificar_produto_carrinho_existe(produto)

        # verificar se produto que está no carrinho foi de fato o produto selecionado
        kart_page.verificar_nome_produto(produto)
        
        # remover produto do carrinho
        kart_page.clicar_remover_produto()

        # verificar que produto foi removido
        kart_page.verificar_produto_carrinho_nao_existe(produto)
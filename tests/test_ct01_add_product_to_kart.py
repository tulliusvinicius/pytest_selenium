from pages.login_page import LoginPage
from pages.home_page import HomePage
from pages.kart_page import KartPage
import pytest


@pytest.mark.usefixtures("setup_teardown")
@pytest.mark.carrinho
class TestCT01:

    def test_ct01_add_1_product_to_kart(self):
        
        login_page = LoginPage()
        home_page = HomePage()
        kart_page = KartPage()
        
        produto_1 = "Sauce Labs Fleece Jacket"
        produto_2 = "Sauce Labs Onesie"

        # fazer login
        login_page.fazer_login("standard_user", "secret_sauce")

        # adicionar produto no carrinho
        home_page.adicionar_ao_carrinho(produto_1)

        # verificar que produto foi adicionado
        home_page.acessar_carrinho()
        kart_page.verificar_produto_carrinho_existe(produto_1)

        # verificar se o produto que está no carrinho foi de fato o produto selecionado
        kart_page.verificar_nome_produto(produto_1)

        # clicar para voltar para tela a tela de produtos
        kart_page.clicar_continuar_comprando()

        #adicionar outro produto no carrinho
        home_page.adicionar_ao_carrinho(produto_2)

        #ir no carrinho de novo e verificando que os 2 produtos foram adicionados
        home_page.acessar_carrinho()
        kart_page.verificar_produto_carrinho_existe(produto_1)
        kart_page.verificar_produto_carrinho_existe(produto_2)

        # verificar se os 2 produtos que estão no carrinho são de fato os produtos selecionados
        kart_page.verificar_nome_produto(produto_1)
        #kart_page.verificar_nome_produto(produto_2)

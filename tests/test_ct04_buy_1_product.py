from pages.checkout_complete_page import CheckoutCompletePage
from pages.checkout_one_page import CheckoutPageOne
from pages.checkout_two_page import CheckoutPageTwo
from pages.login_page import LoginPage
from pages.home_page import HomePage
from pages.kart_page import KartPage
import pytest


@pytest.mark.usefixtures("setup_teardown")
@pytest.mark.carrinho
class TestCT04:
    
    def test_ct04_buy_1_product(self):        
        
        login_page = LoginPage()
        home_page = HomePage()
        kart_page = KartPage()
        checkout_one_page = CheckoutPageOne()
        checkout_two_page = CheckoutPageTwo()
        checkout_complete_page = CheckoutCompletePage()
        
        produto = "Sauce Labs Fleece Jacket"
        
        # fazer login
        login_page.fazer_login("standard_user", "secret_sauce")
        
        # fazer uma compra com 1 produto no carrinho

        # clicar no produto e adicionar no carrinho
        home_page.adicionar_ao_carrinho(produto)
        
        # verificar que produto foi adicionado
        home_page.acessar_carrinho()
        kart_page.verificar_produto_carrinho_existe(produto)

        # verificar se produto que está no carrinho foi de fato o produto selecionado
        kart_page.verificar_nome_produto(produto)
        
        # clicar em checkout
        kart_page.clicar_checkout()
        
        # preencher informações pessoais
        checkout_one_page.preencher_informacoes("Teste", "Selenium", "4.4.8")
        checkout_one_page.continue_checkout()
        
        # verificar valor total
        checkout_two_page.verificar_sub_total()
        
        # finalizar compra
        checkout_two_page.finalizar_compra()

        # #verificar que a compra foi feita com sucesso
        checkout_complete_page.verificar_compra_sucesso()

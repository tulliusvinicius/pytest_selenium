from pages.checkout_complete_page import CheckoutCompletePage
from pages.checkout_one_page import CheckoutPageOne
from pages.checkout_two_page import CheckoutPageTwo
from pages.login_page import LoginPage
from pages.home_page import HomePage
from pages.kart_page import KartPage
import pytest


@pytest.mark.usefixtures("setup_teardown")
@pytest.mark.carrinho
class TestCT05:
    
    def test_ct05_buy_2_products(self):
        
        login_page = LoginPage()
        home_page = HomePage()
        kart_page = KartPage()
        checkout_one_page = CheckoutPageOne()
        checkout_two_page = CheckoutPageTwo()
        checkout_complete_page = CheckoutCompletePage()
        
        produto_1 = "Sauce Labs Fleece Jacket"
        produto_2 = "Sauce Labs Backpack"
        
        # fazer login
        login_page.fazer_login("standard_user", "secret_sauce")

        #fazer uma compra com 2 produto no carrinho

        #clicar no produto e adicionar no carrinho
        home_page.adicionar_ao_carrinho(produto_1)

        #ir no carrinho
        home_page.acessar_carrinho()
        kart_page.verificar_produto_carrinho_existe(produto_1)
        
        #clicando para voltar para tela a tela de produtos
        kart_page.clicar_continuar_comprando()
        
        #adicionar outro produto no carrinho
        home_page.adicionar_ao_carrinho(produto_2)
        home_page.acessar_carrinho()
        kart_page.verificar_produto_carrinho_existe(produto_2)

        #ir no carrinho de novo e verificando que os 2 produtos foram adicionados
        home_page.acessar_carrinho()
        kart_page.verificar_produto_carrinho_existe(produto_1)
        kart_page.verificar_produto_carrinho_existe(produto_2)

        # clicar em checkout
        kart_page.clicar_checkout()

        # preencher informações pessoais
        checkout_one_page.preencher_informacoes("Teste", "Selenium", "4.4.8")
        checkout_one_page.continue_checkout()
        
        # verificar valor total
        checkout_two_page.verificar_sub_total()
        
        # finalizar compra
        checkout_two_page.finalizar_compra()

        # #verificar que a compra foi feita com sucesso
        checkout_complete_page.verificar_compra_sucesso()

import conftest
from selenium.webdriver.common.by import By
from pages.base_page import BasePage


class CheckoutCompletePage(BasePage):
    
    def __init__(self):
        self.driver = conftest.driver
        self.message_order_success = (By.XPATH, "//*[text()='Thank you for your order!']")
    
    def verificar_compra_sucesso(self):
        self.verificar_se_elemento_existe(self.message_order_success)

import conftest
from selenium.webdriver.common.by import By
from pages.base_page import BasePage
from pages.kart_page import KartPage


class CheckoutPageTwo(BasePage):
    
    def __init__(self):
        self.driver = conftest.driver
        self.sub_total_price = (By.XPATH, "//*[@class='summary_subtotal_label']")
        self.botao_finalizar = (By.ID, "finish")
        
    def verificar_sub_total(self):
        kart_page = KartPage()
        expected_total_price = kart_page.somar_valor_produtos()
        actual_total_price = self.encontrar_elementos(self.sub_total_price)
        
        for item in range(len(actual_total_price)):
            actual_total_price = actual_total_price[item].text
            actual_total_price = float(actual_total_price.replace("Item total: $", ""))
        assert actual_total_price == expected_total_price, f"O valor sub total retornado foi '${actual_total_price}', mas o valor sub total esperado era '${expected_total_price}'."
        
    def finalizar_compra(self):
        self.clicar(self.botao_finalizar)

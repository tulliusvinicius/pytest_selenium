import conftest
from pages.base_page import BasePage
from selenium.webdriver.common.by import By

class LoginPage(BasePage):

    def __init__(self):
        self.driver = conftest.driver
        self.user_name_field = (By.ID, "user-name")
        self.password_field = (By.ID, "password")
        self.login_button = (By.ID, "login-button")
        self.error_message_login = (By.XPATH, "//*[@data-test='error']")

    def fazer_login(self, usuario, senha):
        self.escrever(self.user_name_field, usuario)
        self.escrever(self.password_field, senha)
        self.clicar(self.login_button)

    def verificar_mensagem_erro_login_existe(self):
        self.verificar_se_elemento_existe(self.error_message_login)

    def verificar_texto_mensagem_erro_login(self, expected_text):
        actual_text = self.pegar_texto_elemento(self.error_message_login)
        assert actual_text == expected_text, f"O texto retornado foi '{actual_text}', mas era esperado o texto '{expected_text}'."
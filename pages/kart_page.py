import conftest
from selenium.webdriver.common.by import By
from pages.base_page import BasePage


class KartPage(BasePage):
    
    def __init__(self):
        self.driver = conftest.driver
        self.item_iventario = (By.XPATH, "//*[@class='inventory_item_name' and text()='{}']")
        self.product_name = (By.XPATH, "//*[@class='inventory_item_name']")
        self.botao_continuar_comprando = (By.ID, "continue-shopping")
        self.botao_checkout = (By.ID, "checkout")
        self.botao_remover = (By.XPATH, "//button[@class='btn btn_secondary btn_small cart_button']")
        self.list_price = (By.XPATH, "//*[@class='inventory_item_price']")
        
    def verificar_produto_carrinho_existe(self, nome_item):
        item = (self.item_iventario[0], self.item_iventario[1].format(nome_item))
        self.verificar_se_elemento_existe(item)
        
    def verificar_produto_carrinho_nao_existe(self, nome_item):
        # ajeitar esse assert
        
        item = (self.item_iventario[0], self.item_iventario[1].format(nome_item))
        self.verificar_se_elemento_nao_existe(item)

    def verificar_nome_produto(self, expected_name):
        actual_name = self.pegar_texto_elemento(self.product_name)
        assert actual_name == expected_name, f"O texto retornado foi '{actual_name}', mas era esperado o texto '{expected_name}'."

    def clicar_continuar_comprando(self):
        self.clicar(self.botao_continuar_comprando)

    def clicar_checkout(self):
        self.clicar(self.botao_checkout)

    # este método captura o valor de cada produto, retira o $ e converte em float para poder ser comparado com o sub_total do método 'verificar_sub_total'
    def somar_valor_produtos(self):
        prices = self.encontrar_elementos(self.list_price)
        
        expected_total_prices = 0
        for item in range(len(prices)):
            price = prices[item].text
            price = float(price.replace("$", ""))
            expected_total_prices += price
        return expected_total_prices
    
    def clicar_remover_produto(self):
        # botao = self.encontrar_elementos(self.botao_remover)
        # botao[0].click()
        self.clicar(self.botao_remover)
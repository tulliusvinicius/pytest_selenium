import conftest
from selenium.webdriver.common.by import By
from pages.base_page import BasePage


class CheckoutPageOne(BasePage):

    def __init__(self):
        self.driver = conftest.driver
        self.first_name_field = (By.ID, "first-name")
        self.last_name_field = (By.ID, "last-name")
        self.zip_code_field = (By.ID, "postal-code")
        self.botao_continue = (By.ID, "continue")

    def preencher_informacoes(self, first_name, last_name, zip_code):
        self.escrever(self.first_name_field, first_name)
        self.escrever(self.last_name_field, last_name)
        self.escrever(self.zip_code_field, zip_code)

    def continue_checkout(self):
        self.clicar(self.botao_continue)